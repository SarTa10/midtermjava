<%@ page import="javax.persistence.EntityManagerFactory" %>
<%@ page import="javax.persistence.EntityManager" %>
<%@ page import="javax.persistence.Persistence" %>
<%@ page import="javax.persistence.EntityTransaction" %>
<%@ page import="com.example.demo.Data" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix = "c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix = "sql"%>


<!DOCTYPE html>
<%

    EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("default");

    EntityManager entityManager = entityManagerFactory.createEntityManager();

    EntityTransaction entityTransaction = entityManager.getTransaction();

    try {
        entityTransaction.begin();
        if(request.getParameter("product_name")!=null && request.getParameter("amount")!=null && request.getParameter("Base_cost")!=null && request.getParameter("owner")!=null) {
            Data data = new Data();
            data.setProductName(request.getParameter("product_name"));
            data.setAmount(Long.valueOf(request.getParameter("amount")));
            data.setBaseCost(Long.valueOf(request.getParameter("Base_cost")));
            data.setOwner(request.getParameter("owner"));

            entityManager.persist(data);
            entityTransaction.commit();
        }
    } finally {
        if (entityTransaction.isActive()) {
            entityTransaction.rollback();
        }
        entityManager.close();
        entityManagerFactory.close();
    }

%>
<html>
<head>
    <title>JSP - Hello World</title>
</head>
<body>
<h1><%= "Hello World!" %>
</h1>
<br/>
<a href="hello-servlet">Hello Servlet</a>

<a href="Data.jsp"> data.jsp</a>

<sql:setDataSource var = "ug" driver = "com.mysql.jdbc.Driver"
                   url = "jdbc:mysql://localhost/ug"
                   user = "root"  password = "123456"/>

<sql:query dataSource = "${ug}" var = "result">
    SELECT * from DATA;
</sql:query>

<table border = "1" width = "100%">
    <tr>
        <th>Name</th>
    </tr>

    <c:forEach var = "row" items = "${result.rows}">
        <tr>
            <td><c:out value = "${row.ID}"/></td>
            <td><c:out value="${row.PRODUCT_NAME}" /></td>
            <td><c:out value="${row.AMOUNT}"/></td>
            <td><c:out value="${row.BASE_COST}"/></td>
            <tb><c:out value="${row.OWNER}"/></tb>

        </tr>
    </c:forEach>
</table>
</body>
</html>



