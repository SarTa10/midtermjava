package com.example.demo;

import javax.persistence.*;

@Entity
@Table(name="DATA")
public class Data {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="ID")
    private long id;

    @Column(name="PRODUCT_NAME")
    private String productName;

    @Column(name="AMOUNT")
    private long amount;

    @Column(name="BASE_COST")
    private long baseCost;

    @Column(name="OWNER")
    private String owner;

    public Data() {
    }

    public Data(long id, String productName, long amount, long baseCost, String owner) {
        this.id = id;
        this.productName = productName;
        this.amount = amount;
        this.baseCost = baseCost;
        this.owner = owner;
    }

    @Override
    public String toString() {
        return "Data{" +
                "id=" + id +
                ", productName='" + productName + '\'' +
                ", amount=" + amount +
                ", baseCost=" + baseCost +
                ", owner='" + owner + '\'' +
                '}';
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    public long getBaseCost() {
        return baseCost;
    }

    public void setBaseCost(long baseCost) {
        this.baseCost = baseCost;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }
}
