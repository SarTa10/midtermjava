
import com.example.demo.Data;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class test {

    private static EntityManagerFactory entityManagerFactory;
    private static EntityManager entityManager;
    private static EntityTransaction entityTransaction;

    @BeforeAll
    static void beforeAll() {

        entityManagerFactory = Persistence.createEntityManagerFactory("default");
        entityManager = entityManagerFactory.createEntityManager();
        entityTransaction = entityManager.getTransaction();

    }

    @BeforeEach
    void beforeEach() {

        entityTransaction.begin();

    }

    @Test
    void Test(){

        Data data = new Data();

        data.setProductName("prdct");
        data.setAmount(23);
        data.setBaseCost(12);
        data.setOwner("naruto");

        entityManager.persist(data);



    }

    static Data data() {
        return new Data(1,"prdct",23,12,"naruto");
    }

    @ParameterizedTest
    @MethodSource("data")
    void test_MethodSource_Objects(Data b) {
        assertNotNull(b.getOwner());
    }

    @ParameterizedTest
    @MethodSource("data")
    void test_MethodSource_Objects(Data b) {
        assertNotNull(b.getBaseCost());
    }


    @AfterEach
    void afterEach() {


        entityTransaction.commit();

    }

    @AfterAll
    static void afterAll() {


        entityManager.close();
        entityManagerFactory.close();

    }

}
